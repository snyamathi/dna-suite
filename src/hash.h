#ifndef HASH_H_INCLUDED
#define HASH_H_INCLUDED

void populateHashTable(const genomeDebug* genome, const unsigned int* octetCounts, unsigned int** hashTable);

void findMatchFromHashTable(const genomeDebug* genome, unsigned int** hashTable, const unsigned int* numberOfPointers, readDebug* read);

#endif
