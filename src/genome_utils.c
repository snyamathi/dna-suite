#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <math.h>
#include "mt19937-64.h"
#include "sn_errors.h"
#include "genome_utils.h"
#include "burrows_wheeler.h"
#include "listutils.h"

unsigned int basesToInt(const char* sequence, const int length, unsigned int value)
{
	int i;
	for (i = 0; i < length; ++i) {
		value <<= 2;
		switch(sequence[i]) {
            case 'A':
                // value += 0;
                break;
            case 'C':
                value += 1;
                break;
            case 'G':
                value += 2;
                break;
            case 'T':
                value += 3;
                break;
		}
	}
	return value;
}

unsigned getBitsForBase(const char base)
{
	switch(base) {
	case 'A':
		return 0;
	case 'C':
		return 1;
	case 'G':
		return 2;
	case 'T':
		return 3;
	}
	return -1;
}

unsigned short basesToShort(const char* sequence, const int length, unsigned short value)
{
	int i;
	for (i = 0; i < length; ++i) {
		value <<= 2;
		switch(sequence[i]) {
            case 'A':
                // value += 0;
                break;
            case 'C':
                value += 1;
                break;
            case 'G':
                value += 2;
                break;
            case 'T':
                value += 3;
                break;
		}
	}
	return value;
}

char getCharForShort(const unsigned short value)
{
	switch (value) {
	case 0:
		return 'A';
	case 1:
		return 'C';
	case 2:
		return 'G';
	case 3:
		return 'T';
	}
	return -1;
}

void getQuartetForBits(const unsigned short value, char quartet[])
{
	quartet[3] = getCharForShort((value & 0x300) >> 8);
	quartet[2] = getCharForShort((value & 0xC00) >> 10);
	quartet[1] = getCharForShort((value & 0x3000) >> 12);
	quartet[0] = getCharForShort((value & 0xC000) >> 14);
}

void getOctetForBits(const unsigned short value, char octet[])
{
	octet[7] = getCharForShort(value & 0x3);
	octet[6] = getCharForShort((value & 0xC) >> 2);
	octet[5] = getCharForShort((value & 0x30) >> 4);
	octet[4] = getCharForShort((value & 0xC0) >> 6);
	octet[3] = getCharForShort((value & 0x300) >> 8);
	octet[2] = getCharForShort((value & 0xC00) >> 10);
	octet[1] = getCharForShort((value & 0x3000) >> 12);
	octet[0] = getCharForShort((value & 0xC000) >> 14);
}

void countBases(genomeDebug* g)
{
	g->countA = 0;
	g->countC = 0;
	g->countG = 0;
	g->countT = 0;

	unsigned int i;
	for (i = 0; i < g->genomeLength; ++i) {
		switch(g->genomeString[i]) {
		case 'A':
			++g->countA;
			break;
		case 'C':
			++g->countC;
			break;
		case 'G':
			++g->countG;
			break;
		case 'T':
			++g->countT;
			break;
		}
	}
}

void printOctetsToFile(const unsigned int* octetCounts, const char* filename)
{
	FILE* octetCountsFile;
	octetCountsFile = fopen(filename, "w");
	checkFile(octetCountsFile);

	fprintf(octetCountsFile, "%i", octetCounts[0]);

	int i;
	for (i = 1; i < 65536; ++i) {
		fprintf(octetCountsFile, "\n%i", octetCounts[i]);
	}

	fclose(octetCountsFile);
}

void countOctetsInGenome(const char* genome, const unsigned int genomeLength, unsigned int** octetCounts)
{
	*octetCounts = (unsigned int*) calloc(65536, sizeof(int));
	checkMalloc(*octetCounts);

	unsigned int i;
	for (i = 0; i < genomeLength-7; ++i) {
		(*octetCounts)[basesToShort(genome+i, 8, 0)] += 1;
	}

	unsigned int sum = 0;
	for (i = 0; i < 65536; ++i) {
		sum += (*octetCounts)[i];
	}

	assert(sum == genomeLength -7);
}

void printSegmentsFromGenome(const char* genome, const unsigned int genomeLength, const unsigned int* genomePointers, const unsigned int numberOfPointers, const int limit)
{
	int i = 0;
	int normal = 0;
	int overflow = 0;

	if (numberOfPointers == 0 || limit == 0) {
		printf("[]\n");
		return;
	}

	char* tmp = (char*) malloc((limit+1) * sizeof(char));
	checkMalloc(tmp);
	tmp[limit] = '\0';

	for (i = 0; i < numberOfPointers; ++i) {
		if (genomePointers[i] + limit < genomeLength) {
			memcpy(tmp, genome + genomePointers[i], limit);
		} else {
			overflow = genomePointers[i] + limit - genomeLength;
			normal = limit - overflow;
			memcpy(tmp, genome + genomePointers[i], normal);
			memcpy(tmp + normal, genome, overflow);
		}
		printf("%s\n", tmp);
	}
	free(tmp);
}

void getLocationsOfOctet(const char* genome, const unsigned int genomeLength, const char* octet, unsigned int** genomePointers, unsigned int* numberOfPointers)
{

	unsigned int i;
	unsigned int* temp;
	*numberOfPointers = 0;

	temp = (unsigned int*) malloc(150000 * sizeof (unsigned int));
	checkMalloc(temp);

	for (i = 0; i < genomeLength-7; ++i) {
		if (!memcmp(octet, genome+i, 8)) {
			temp[*numberOfPointers] = i;
			++(*numberOfPointers);
			assert(*numberOfPointers < 125000);
		}
	}

	*genomePointers = (unsigned int*) malloc(*numberOfPointers * sizeof(unsigned int));
	checkMalloc(*genomePointers);

	memcpy(*genomePointers, temp, (*numberOfPointers) * sizeof(unsigned int));
	free(temp);
}

void getLocationsOfBase(genomeDebug* g, const char base, unsigned int** genomePointers, unsigned int* numberOfPointers)
{
	unsigned int i;
	unsigned int j;
	switch(base) {
	case '$':
		*numberOfPointers = 1;
		break;
	case 'A':
		*numberOfPointers = g->countA;
		break;
	case 'C':
		*numberOfPointers = g->countC;
		break;
	case 'G':
		*numberOfPointers = g->countG;
		break;
	case 'T':
		*numberOfPointers = g->countT;
		break;
	}

	*genomePointers = (unsigned int*) malloc((*numberOfPointers) * sizeof(unsigned int));
	checkMalloc(*genomePointers);

	for (i = 0, j = 0; i < g->genomeLength; ++i) {
		if (g->genomeString[i] == base) {
			(*genomePointers)[j] = i;
			++j;
		}
	}

	assert(*numberOfPointers == j);
}

void cleanGenome(genomeDebug* g)
{
	unsigned int i;
	unsigned int newGenomeLength;
	char* newGenome;

	newGenomeLength = 0;
	newGenome = (char*) malloc(g->genomeLength * sizeof(char));
	checkMalloc(newGenome);

	for (i = 0; i < g->genomeLength; ++i)
	{
		switch((g->genomeString)[i]) {
		case 'A':
		case 'a':
			newGenome[newGenomeLength++] = 'A';
			break;
		case 'C':
		case 'c':
			newGenome[newGenomeLength++] = 'C';
			break;
		case 'G':
		case 'g':
			newGenome[newGenomeLength++] = 'G';
			break;
		case 'T':
		case 't':
			newGenome[newGenomeLength++] = 'T';
			break;
		}
	}

	free(g->genomeString);

	g->genomeLength = newGenomeLength;
	g->genomeString = (char*) malloc(newGenomeLength * sizeof(char));
	checkMalloc(g->genomeString);

	memcpy(g->genomeString, newGenome, newGenomeLength);
	free(newGenome);
}

void writeGenomeToFile(const char* genome, const unsigned int genomeLength, const char* filepath)
{
	FILE* genomeFile;
	genomeFile = fopen(filepath, "w");
	checkFile(genomeFile);

	fwrite(genome, 1, genomeLength, genomeFile);
	fclose(genomeFile);
}

void getGenomeLength(const char* genomeFilepath, unsigned int* genomeLength)
{
	FILE* genomeFile;
	genomeFile = fopen(genomeFilepath, "r");
	checkFile(genomeFile);

	fseeko64(genomeFile, 0, SEEK_END);
	*genomeLength = ftello64(genomeFile);

	fclose(genomeFile);
}

char getRandomChar()
{
	switch(genrand_int32() % 4) {
	case 0:
		return 'A';
	case 1:
		return 'C';
	case 2:
		return 'G';
	case 3:
		return 'T';
	}
	return 0;
}

void readGenome(const char* genomeFilepath, genomeDebug* g)
{
	// Get the length of the genome file
	FILE* genomeFile;
	size_t result;

	getGenomeLength(genomeFilepath, &g->genomeLength);

	genomeFile = fopen(genomeFilepath, "r");
	checkFile(genomeFile);

	g->genomeString = (char*) malloc(g->genomeLength * sizeof(char));
	checkMalloc(g->genomeString);

	unsigned int i;
	unsigned int charsRead = 0;
	for (i = 0; i < g->genomeLength - 1000000; i += 1000000) {
		result = fread(g->genomeString + i, 1, 1000000, genomeFile);
		checkRead(result, 1000000);
		charsRead += result;
	}

	unsigned int remaining = g->genomeLength - i;
	assert(remaining < 1000000);

	result = fread(g->genomeString + i, 1, remaining, genomeFile);
	checkRead(result, remaining);
	charsRead += result;

	assert(charsRead == g->genomeLength);

	fclose(genomeFile);

	countBases(g);
}

void generateRandomGenome(genomeDebug* g)
{
	int i;
	char bases[4] = {'A', 'C', 'G', 'T'};

	g->genomeString = (char*) malloc(g->genomeLength * sizeof(char));
	checkMalloc(g->genomeString);

	for (i = 0; i < g->genomeLength; ++i) {
		(g->genomeString)[i] = bases[genrand_int32() % 4];
	}

	countBases(g);
}

void generateRandomRead(const char* genome, const unsigned int genomeLength, readDebug* read)
{
	unsigned int offset = genrand_int32() % (genomeLength - read->sequenceLength);
	read->sequence = (char*) malloc(read->sequenceLength * sizeof(char));
	checkMalloc(read->sequence);
	memcpy(read->sequence, genome + offset, read->sequenceLength);
	read->mismatchCount = 0;
}

void generateRandomReadWithErrors(const char* genome, const unsigned int genomeLength, readDebug* read)
{
	read->mismatchCount = 0;

	unsigned int offset = genrand_int32() % (genomeLength - read->sequenceLength);
	read->sequence = (char*) malloc(read->sequenceLength * sizeof(char));
	checkMalloc(read->sequence);
	memcpy(read->sequence, genome + offset, read->sequenceLength);

	unsigned int snp1Location = genrand_int32() % (read->sequenceLength);
	read->snp1 = getRandomChar();
	if (read->sequence[snp1Location] != read->snp1) {
		++(read->mismatchCount);
	}
	read->snp1Location = snp1Location + offset;
	read->sequence[snp1Location] = read->snp1;


	unsigned int snp2Location = snp1Location;
	while (snp2Location == snp1Location) {
		snp2Location = genrand_int32() % (read->sequenceLength);
	}
	read->snp2 = getRandomChar();
	if (read->sequence[snp2Location] != read->snp2) {
		++(read->mismatchCount);
	}
	read->snp2Location = snp2Location + offset;
	read->sequence[snp2Location] = read->snp2;
}

void simulateReads(const char* genomeFilepath, const char* readsFilepath, const int readLength, const int coverage)
{
	// Get the length of the genome and ensure that it's not smaller than the desired read length
	unsigned int genomeLength = 0;
	getGenomeLength(genomeFilepath, &genomeLength);

	if (readLength > genomeLength) {
		fputs("ERR: Read length > genome length", stderr);
		exit(1);
	}

	// Open the genome file and file that we're going to put the reads into
	FILE* genomeFile;
	genomeFile = fopen(genomeFilepath, "r");
	checkFile(genomeFile);

	FILE* readsFile;
	readsFile = fopen(readsFilepath, "w");
	checkFile(readsFile);

	// Determine the number of reads we need and the valid range from which to generate reads
	int numReads = (genomeLength * coverage) / readLength;
	int max = genomeLength - readLength;

	// Buffer for reads
	char read[readLength+1];
	read[readLength] = '\0';

	// Generate a single read
	fseek(genomeFile, genrand_int32() % max, SEEK_SET);
	fread(read, 1, readLength, genomeFile);
	fputs(read, readsFile);

	// Then generate the rest of the reads (with newline)
	int i;
	for (i = 0; i < numReads; ++i) {
		fseek(genomeFile, genrand_int32() % max, SEEK_SET);
		fread(read, 1, readLength, genomeFile);
		fprintf(readsFile, "\n%s", read);
	}

	// Clean up
	fclose(readsFile);
	fclose(genomeFile);
}
