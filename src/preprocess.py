import itertools
import sys
  
def getIterationsOfBasesAsDict():
    bases = ['A', 'C', 'G', 'T']
    iterations = itertools.product(bases, repeat=8)
    dict = {}
    for i in iterations:
        dict[''.join(i)] = 0
    return dict

def count(filepath):
    dict = getIterationsOfBasesAsDict()
    f = open(filepath)
    current_bases = []
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    while True:
        c = f.read(1)
        if not c:
            print 'EOF'
            break
        current_bases.append(c)
        dict[''.join(current_bases)] += 1
        current_bases = current_bases[1:]
    f.close()
    writeCountsToFile('H:\\TEMP\\counts.csv', dict)
	
def writeCountsToFile(filepath, dict):
    keys = dict.keys()
    keys.sort()    
    with open(filepath, 'w+') as f:
        for key in keys:
            f.write(str(key))
            f.write(',')
            f.write(str(dict[key]))
            f.write('\n')
			
count(sys.argv[1])