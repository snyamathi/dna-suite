#include <stdlib.h>
#include "genome_utils.h"
#include "listutils.h"
#include <limits.h>
#include "sn_errors.h"
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <memory.h>

void populateHashTable(const genomeDebug* genome, const unsigned int* octetCounts, unsigned int** hashTable)
{
	unsigned short key;
	unsigned int i;
	unsigned int* tmpCount;

	tmpCount = (unsigned int*) malloc(65536 * sizeof(unsigned int));
	checkMalloc(tmpCount);

	for (i = 0; i < 65536; ++i) {
		hashTable[i] = malloc(octetCounts[i] * sizeof(unsigned int));
		checkMalloc(hashTable[i]);
		tmpCount[i] = 0;
	}

	for (i = 0; i < genome->genomeLength - 7; ++i) {
		key = basesToShort(genome->genomeString + i, 8, 0);
		hashTable[key][tmpCount[key]] = i;
		++(tmpCount[key]);
	}

	assert(!memcmp(octetCounts, tmpCount, 65536 * sizeof(unsigned int)));
	free(tmpCount);
}

void getListsFromSequence(unsigned int** hashTable, const readDebug* read, unsigned int** lists, const unsigned int* octetCounts, unsigned int* listCounts, unsigned int* totalCount)
{
	*totalCount = 0;
	unsigned int i;
	unsigned short key;
	for (i = 0; i < read->sequenceLength - 7; ++i) {
		key = basesToShort(read->sequence + i, 8, 0);

		lists[i] = (unsigned int*) malloc(octetCounts[key] * sizeof(unsigned int));
		checkMalloc(lists[i]);

		memcpy(lists[i], hashTable[key], octetCounts[key] * sizeof(unsigned int));
		listCounts[i] = octetCounts[key];
		*totalCount += octetCounts[key];
	}
}

void findMatchFromHashTable(const genomeDebug* genome, unsigned int** hashTable, const unsigned int* octetCounts, readDebug* read)
{
	unsigned int i;
	unsigned int** lists;
	unsigned int totalCount;
	unsigned int* masterList;
	unsigned int matchLocation;
	unsigned int modeCount;
	unsigned int* listCounts;
	unsigned int numberOfLists = read->sequenceLength - 7;
	read->mismatchCount = 0;

	listCounts = (unsigned int*) malloc(numberOfLists * sizeof(unsigned int));
	checkMalloc(listCounts);

	lists = (unsigned int**) malloc(numberOfLists * sizeof(unsigned int*));
	checkMalloc(lists);

	getListsFromSequence(hashTable, read, lists, octetCounts, listCounts, &totalCount);

	masterList = (unsigned int*) malloc(totalCount * sizeof(unsigned int));
	checkMalloc(masterList);

	mergeWithOffset(lists, listCounts, totalCount, read->sequenceLength - 7, masterList);
	getModeOfListUInt(masterList, totalCount, &matchLocation, &modeCount);

	if (matchLocation == INT_MAX) {
		read->mismatchCount = INT_MAX;
	} else {
		int firstDiffFound = false;

		for (i = 0; i < read->sequenceLength; ++i) {
			if (genome->genomeString[matchLocation + i] == read->sequence[i]) {
				continue;
			}

			if (!firstDiffFound) {
				read->snp1 = read->sequence[i];
				read->snp1Location = matchLocation + i;
				firstDiffFound = true;
			} else {
				read->snp2 = read->sequence[i];
				read->snp2Location = matchLocation + i;
			}

			++read->mismatchCount;
		}
	}

	for (i = 0; i < read->sequenceLength - 7; ++i) {
		if (lists[i] != 0) {
			free(lists[i]);
		}
	}

	free(lists);
	free(listCounts);
	free(masterList);
}
