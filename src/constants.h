#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

extern const char* testGenomeFilepath;

extern const char* readsFilepath;

extern const char* octetCountsFilepath;

extern const char* randomGenomeFilepath100M;

extern const char* randomGenomeFilepath10M;

extern const char* randomGenomeFilepath1M;

extern const char* randomGenomeFilepath10K;

extern const char* logFilePath;

extern int countRate;

extern int indexRate;

#endif
