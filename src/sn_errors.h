#ifndef SN_ERRORS_H_INCLUDED
#define SN_ERRORS_H_INCLUDED

#define checkMalloc(pointer) _checkMalloc(pointer, __LINE__, __FILE__)
#define checkFile(pointer) _checkFile(pointer, __LINE__, __FILE__)
#define checkRead(observed, expected) _checkRead(observed, expected, __LINE__, __FILE__)

void _checkMalloc(const void* pointer, const int lineNumber, const char* filename);

void _checkFile(const void* pointer, const int lineNumber, const char* filename);

void _checkRead(const unsigned int observed, const unsigned int expected, const int lineNumber, const char* filename);

#endif
