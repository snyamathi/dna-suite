import itertools

def concat_and_clean():
  chrFilenames = ['chr%d.fa' % i for i in range(1, 23)]
  chrFilenames.append('chrX.fa')
  chrFilenames.append('chrY.fa')
  with open('master.fa', 'w+') as master:
      for chrFilename in chrFilenames:
        with open(chrFilename, 'r') as chrFile:
          processFile(master, chrFile)

def processFile(master, chrFile):
  bases = {'A':1, 'a':1, 'C':1, 'c':1, 'G':1, 'g':1, 'T':1, 't':1}
  while True:
    c = chrFile.read(1)
    if not c:
      print 'Finished with ' + chrFile.name
      break
    if bases.has_key(c):
      master.write(c.upper())

def getIterationsOfBasesAsDict():
  bases = ['A', 'C', 'G', 'T']
  iterations = itertools.product(bases, repeat=8)
  dict = {}
  for i in iterations:
      dict[''.join(i)] = 0
  return dict

def count(filepath):
  dict = getIterationsOfBasesAsDict()
  f = open(filepath)
  current_bases = []
  current_bases.append(f.read(1))
  current_bases.append(f.read(1))
  current_bases.append(f.read(1))
  current_bases.append(f.read(1))
  current_bases.append(f.read(1))
  current_bases.append(f.read(1))
  current_bases.append(f.read(1))
  while True:
    c = f.read(1)
    if not c:
      print 'EOF'
      break
    current_bases.append(c)
    dict[''.join(current_bases)] += 1
    current_bases = current_bases[1:]
  f.close()
  writeCountsToFile('counts.txt', dict)

def writeCountsToFile(filepath, dict):
  keys = dict.keys()
  keys.sort()
  with open(filepath, 'w+') as f:
    for key in keys:
      f.write(str(key))
      f.write(',')
      f.write(str(dict[key]))
      f.write('\n')

def writeLocationsToFile():
  bases = ['A', 'C', 'G', 'T']
  iterations = itertools.product(bases, repeat=8)
  dict = {}
  index = 0
  for i in iterations:
    dict[''.join(i)] = []
  current_bases = []
  with open('maste.fa', 'r') as master:
    current_bases.append(master.read(1))
    current_bases.append(master.read(1))
    current_bases.append(master.read(1))
    current_bases.append(master.read(1))
    current_bases.append(master.read(1))
    current_bases.append(master.read(1))
    current_bases.append(master.read(1))
    while True:
      c = master.read(1)
      if not c:
        print 'EOF'
        break
      current_bases.append(c)
      dict[''.join(current_bases)].append(index)
      current_bases = current_bases[1:]
      index += 1
    for key in dict.keys():
        with open(key, 'w+') as seqFile:
            for location in dict[key]:
                seqFile.write(str(location))
                seqFile.write('\n')
      
writeLocationsToFile()