#ifndef LIST_UTILS_H_INCLUDED
#define LIST_UTILS_H_INCLUDED

int* getMasterList(int** lists, int sequenceLength);

void printLists(const unsigned int** lists, const unsigned int* counts, const unsigned int sequenceLength);

void printListUInt(unsigned int* list, unsigned int count);

void getModeOfListUInt(unsigned int* list, unsigned int count, unsigned int* mode, unsigned int* modeCount);

int** getLists();

void mergeWithOffset(unsigned int** lists, const unsigned int* counts, unsigned int totalCount, const unsigned int numberOfLists, unsigned int* masterList);

unsigned int partitionUInt(unsigned int* array, unsigned int left, unsigned int right, unsigned int pivotIndex);

void quicksortUInt(unsigned int* array, unsigned int left, unsigned int right);

#endif
