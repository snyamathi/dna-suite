#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include "genome_utils.h"

unsigned int seqcomp(const char* reference, const unsigned int offset, readDebug* read)
{

	// Check if they are identical
	if (!memcmp(reference + offset, read->sequence, read->sequenceLength * sizeof(char))) {
		read->mismatchCount = 0;
		return 0;
	}

	// Otherwise loop through so we can count the mismatches
	char snp1 = '\0';
	char snp2 = '\0';
	unsigned int i;
	unsigned int snp1Location = 0;
	unsigned int snp2Location = 0;
	unsigned int mismatchCount = 0;
	for (i = 0; i < read->sequenceLength; ++i) {
		if (reference[i + offset] == read->sequence[i]) {
			continue;
		}

		++mismatchCount;
		if (mismatchCount == 1) {
			snp1 = read->sequence[i];
			snp1Location = i + offset;
		} else if (mismatchCount == 2) {
			snp2 = read->sequence[i];
			snp2Location = i + offset;
		} else {
			return 1;
		}
	}


	if (mismatchCount > read->mismatchCount) {
		return 1;
	} else {
		read->mismatchCount = mismatchCount;
	}

	if (mismatchCount == 1) {
		read->snp1 = snp1;
		read->snp1Location = snp1Location;
	} else if (mismatchCount == 2) {
		read->snp1 = snp1;
		read->snp1Location = snp1Location;
		read->snp2 = snp2;
		read->snp2Location = snp2Location;
	} else {
		fputs("ERR: Invalid mismatchCount", stderr);
		exit(6);
	}

	return 1;
}

void findMatchTrivial(const genomeDebug* genome, readDebug* read)
{
	read->mismatchCount = UINT_MAX;
	unsigned int i;
	for (i = 0; i < genome->genomeLength - read->sequenceLength + 1; ++i) 	{
		if (!seqcomp(genome->genomeString, i, read)) {
			break;
		}
	}
}
