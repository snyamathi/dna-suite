#include "genome_utils.h"
#include <assert.h>
#include <memory.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "burrows_wheeler.h"
#include "trivial.h"
#include "hash.h"
#include "constants.h"
#include "listutils.h"
#include "sn_errors.h"

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
    LARGE_INTEGER li;
    if(!QueryPerformanceFrequency(&li)) {
    	fputs("QueryPerformanceFrequency failed!\n", stderr);
    	exit(7);
    }

    PCFreq = ((double) li.QuadPart)/1000.0;

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;
}

double GetCounter()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return ((double)(li.QuadPart-CounterStart))/PCFreq;
}

unsigned int readWasAccurate(readDebug* input, readDebug* output)
{
	if (input->mismatchCount != output->mismatchCount) {
		return 0;
	} else if (input->mismatchCount == 0 && output->mismatchCount == 0) {
		return 1;
	}

	if (input->mismatchCount == 1 && (input->snp1 == output->snp1 && input->snp1Location == output->snp1Location)) {
		return 1;
	}

	if (input->mismatchCount == 2 && (
			((input->snp1 == output->snp1 && input->snp1Location == output->snp1Location) && (input->snp2 == output->snp2 && input->snp2Location == output->snp2Location)) ||
			((input->snp1 == output->snp2 && input->snp1Location == output->snp2Location) && (input->snp2 == output->snp1 && input->snp2Location == output->snp1Location)))) {
		return 1;
	}

	return 0;
}

void runReference(const unsigned int which)
{
	FILE* outFile;
	outFile = fopen(logFilePath, "a");
	checkFile(outFile);

	genomeDebug genome;
	readGenome("C:\\master.fa", &genome);

	readDebug read;
	readDebug test;
	read.sequenceLength = 30;

	char* bwtString;
	bwtIndexItem* bwtCountIndex;
	unsigned int* bwtLocationIndex;
	unsigned int** hashTable = NULL;
	unsigned int* octetCounts;
	unsigned int j;
	unsigned int k;
	double end = 0;

	switch (which) {
	case 1:
		genome.genomeString[genome.genomeLength-1] = '$';
		countBases(&genome);
		bwtFull(&genome, &bwtString, &bwtCountIndex, &bwtLocationIndex);
		break;
	case 2:
		hashTable = (unsigned int**) malloc(65536 * sizeof(unsigned int*));
		checkMalloc(*hashTable);
		countOctetsInGenome(genome.genomeString, genome.genomeLength, &octetCounts);
		populateHashTable(&genome, octetCounts, hashTable);
		break;
	}

	for (k = 0; k < 1000; ++k) {
		generateRandomReadWithErrors(genome.genomeString, genome.genomeLength, &read);

		test.snp1 = read.snp1;
		test.snp2 = read.snp2;
		test.snp1Location = read.snp1Location;
		test.snp2Location = read.snp2Location;
		test.mismatchCount = read.mismatchCount;

		read.snp1 = '\0';
		read.snp2 = '\0';
		read.snp1Location = UINT_MAX;
		read.snp2Location = UINT_MAX;
		read.mismatchCount = 0;

		switch (which) {
		case 0:
			StartCounter();
			findMatchTrivial(&genome, &read);
			end = GetCounter();
			break;
		case 1:
			StartCounter();
			getDeltaFromGenomeCompressed(&genome, bwtString, bwtCountIndex, bwtLocationIndex, &read);
			end = GetCounter();
			break;
		case 2:
			StartCounter();
			findMatchFromHashTable(&genome, hashTable, octetCounts, &read);
			end = GetCounter();
			break;
		}

		fprintf(outFile, "%u,%u,%i,%i,%i,%i,%f\n", 0, k, which, indexRate, countRate, readWasAccurate(&read, &test), end);
		printf("%u,%u,%i,%i,%i,%i,%f\n", 0, k, which, indexRate, countRate, readWasAccurate(&read, &test), end);
		fflush(stdout);
		free(read.sequence);
	}

	switch (which) {
	case 1:
		free(bwtString);
		free(bwtCountIndex);
		free(bwtLocationIndex);
		break;
	case 2:
		for (j = 0; j < 65536; ++j) {
			free(hashTable[j]);
		}
		free(hashTable);
		free(octetCounts);
		break;
	}

	free(genome.genomeString);
	fclose(outFile);
}

void runTest(const unsigned int which)
{
	FILE* outFile;
	outFile = fopen(logFilePath, "a");
	checkFile(outFile);

	genomeDebug genome;
	readDebug read;
	readDebug test;
	read.sequenceLength = 30;

	char* bwtString;
	bwtIndexItem* bwtCountIndex;
	unsigned int* bwtLocationIndex;
	unsigned int** hashTable = NULL;
	unsigned int* octetCounts;
	unsigned int i;
	unsigned int j;
	unsigned int k;
	double end = 0;

	for (i = 300; i <= 3000000000u; i *= 10) {
		switch (which) {
		case 0:
			genome.genomeLength = i;
			generateRandomGenome(&genome);
			break;
		case 1:
			genome.genomeLength = i;
			generateRandomGenome(&genome);
			genome.genomeString[i-1] = '$';
			countBases(&genome);

			bwtFull(&genome, &bwtString, &bwtCountIndex, &bwtLocationIndex);
			break;
		case 2:
			genome.genomeLength = i;
			generateRandomGenome(&genome);
			hashTable = (unsigned int**) malloc(65536 * sizeof(unsigned int*));
			checkMalloc(*hashTable);
			countOctetsInGenome(genome.genomeString, genome.genomeLength, &octetCounts);
			populateHashTable(&genome, octetCounts, hashTable);
			break;
		}

		for (k = 0; k < 1000; ++k) {
			generateRandomReadWithErrors(genome.genomeString, genome.genomeLength, &read);

			test.snp1 = read.snp1;
			test.snp2 = read.snp2;
			test.snp1Location = read.snp1Location;
			test.snp2Location = read.snp2Location;
			test.mismatchCount = read.mismatchCount;

			read.snp1 = '\0';
			read.snp2 = '\0';
			read.snp1Location = UINT_MAX;
			read.snp2Location = UINT_MAX;
			read.mismatchCount = 0;

			switch (which) {
			case 0:
				StartCounter();
				findMatchTrivial(&genome, &read);
				end = GetCounter();
				break;
			case 1:
				StartCounter();
				getDeltaFromGenomeCompressed(&genome, bwtString, bwtCountIndex, bwtLocationIndex, &read);
				end = GetCounter();
				break;
			case 2:
				StartCounter();
				findMatchFromHashTable(&genome, hashTable, octetCounts, &read);
				end = GetCounter();
				break;
			}

			fprintf(outFile, "%u,%u,%i,%i,%i,%i,%f\n", i, k, which, indexRate, countRate, readWasAccurate(&read, &test), end);
			printf("%u,%u,%i,%i,%i,%i,%f\n", i, k, which, indexRate, countRate, readWasAccurate(&read, &test), end);
			fflush(stdout);
			free(read.sequence);
		}

		switch (which) {
		case 1:
			free(bwtString);
			free(bwtCountIndex);
			free(bwtLocationIndex);
			break;
		case 2:
			for (j = 0; j < 65536; ++j) {
				free(hashTable[j]);
			}
			free(hashTable);
			free(octetCounts);
			break;
		}

		free(genome.genomeString);
	}

	fclose(outFile);
}

void runTrivialReference()
{
	runReference(0);
}

void runBwtReference()
{
	int indexRates[] = {3,   4,  5,  6,  7,  8,  9, 11, 13, 20, 48};
	int countRates[] = {77, 29, 21, 18, 16, 15, 14, 13, 12, 11, 10};

	unsigned int i;
	for (i = 0; i < 11; ++i) {
		indexRate = indexRates[i];
		countRate = countRates[i];
		runReference(1);
	}
}

void runHashReference()
{
	runReference(2);
}

void runTrivialTest()
{
	runTest(0);
}

void runBwtTest()
{
	int indexRates[] = {3,   4,  5,  6,  7,  8,  9, 11, 13, 20, 48};
	int countRates[] = {77, 29, 21, 18, 16, 15, 14, 13, 12, 11, 10};

	unsigned int i;
	for (i = 0; i < 11; ++i) {
		indexRate = indexRates[i];
		countRate = countRates[i];
		runTest(1);
	}
}

void runHashTest()
{
	runTest(2);
}
