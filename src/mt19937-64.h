#ifndef MT19937_64_H_INCLUDED
#define MT19937_64_H_INCLUDED

void init_genrand_time();

void init_genrand(unsigned long s);

unsigned long genrand_int32(void);

#endif
