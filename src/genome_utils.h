#ifndef GENOME_GENERATOR_H_INCLUDED
#define GENOME_GENERATOR_H_INCLUDED

typedef struct {
	char* genomeString;
	unsigned int genomeLength;
	unsigned int countA;
	unsigned int countC;
	unsigned int countG;
	unsigned int countT;
} genomeDebug;

typedef struct {
	char snp1;
	unsigned int snp1Location;
	char snp2;
	unsigned int snp2Location;
	char* sequence;
	unsigned int sequenceLength;
	unsigned int mismatchCount;
} readDebug;

void getOctetForBits(const unsigned short value, char octet[]);

char getCharForShort(const unsigned short value);

void getLocationsOfOctet(const char* genome, const unsigned int genomeLength, const char* octet, unsigned int** genomePointers, unsigned int* numberOfPointers);

char getRandomChar();

void generateRandomReadWithErrors(const char* genome, const unsigned int genomeLength, readDebug* read);

void countBases(genomeDebug* g);

void printOctetsToFile(const unsigned int* octetCounts, const char* filename);

void countOctetsInGenome(const char* genome, const unsigned int genomeLength, unsigned int** octetCounts);

void printSegmentsFromGenome(const char* genome, const unsigned int genomeLength, const unsigned int* genomePointers, const unsigned int numberOfPointers, const int limit);

void getLocationsOfBase(genomeDebug* g, const char base, unsigned int** genomePointers, unsigned int* numberOfPointers);

void cleanGenome(genomeDebug* g);

void writeGenomeToFile(const char* genome, const unsigned int genomeLength, const char* filepath);

void getGenomeLength(const char* genomeFilepath, unsigned int* genomeLength);

void readGenome(const char* genomeFilepath, genomeDebug* g);

void generateRandomGenome(genomeDebug* g);

void generateRandomRead(const char* genome, const unsigned int genomeLength, readDebug* read);

void simulateReads(const char* genomeFilepath, const char* readsFilepath, const int readLength, const int coverage);

unsigned short basesToShort(const char* sequence, const int length, unsigned short value);

unsigned int basesToInt(const char* sequence, const int length, unsigned int value);

#endif
