#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "listutils.h"
#include "sn_errors.h"
#include "mt19937-64.h"
#include <assert.h>

void getModeOfListUInt(unsigned int* list, unsigned int count, unsigned int* mode, unsigned int* modeCount)
{
    *mode = 0;
    *modeCount = 0;
    
    int i;
    int current = 0;
    int currentCount = 0;
    for (i = 0; i < count; ++i) {
        if (list[i] == current) {
            ++currentCount;
        } else {
            current = list[i];
            currentCount = 1;
        }

        if (currentCount > *modeCount) {
            *mode = current;
            *modeCount = currentCount;
        }
    }
}

void mergeWithOffset(unsigned int** lists, const unsigned int* counts, unsigned int totalCount, const unsigned int numberOfLists, unsigned int* masterList)
{
    unsigned int i;
    unsigned int j;
    unsigned int smallestNumber;
    unsigned int smallestNumberLocation;

    unsigned int* indexes;
    indexes = (unsigned int*) calloc(numberOfLists, sizeof(unsigned int));
    checkMalloc(indexes);

    for (i = 0; i < totalCount; ++i) {
        smallestNumber = UINT_MAX;
        smallestNumberLocation = -1;
        for (j = 0; j < numberOfLists; ++j) {
            if (indexes[j] < counts[j] && (lists[j][indexes[j]] - j <= smallestNumber || lists[j][indexes[j]] - j > 4000000000u)) {
                smallestNumber = lists[j][indexes[j]] - j;
                smallestNumberLocation = j;
             }
        }

        masterList[i] = smallestNumber;
        ++indexes[smallestNumberLocation];
    }
}

void printListUInt(unsigned int* list, unsigned int count)
{
	if (count == 0) {
		fputs("[]\n", stdout);
		fflush(stdout);
		return;
	}

    int i;
    printf("[%u", list[0]);
    for (i = 1; i < count; ++i) {
        printf(", %u", list[i]);
    }
    fputs("]\n", stdout);
	fflush(stdout);
}

void printLists(const unsigned int** lists, const unsigned int* counts, const unsigned int sequenceLength)
{
    int i;
    int j;
    for (i = 0; i < sequenceLength; ++i) {
        for (j = 0; j < counts[i]; ++j) {
            printf("[%i][%i] = %i\n", i, j, lists[i][j]);
        }
    }
}

unsigned int partitionUInt(unsigned int* array, unsigned int left, unsigned int right, unsigned int pivotIndex)
{
	unsigned int pivotValue = array[pivotIndex];

	// Move the pivot to the end
	unsigned int tmp = array[right];
	array[right] = array[pivotIndex];
	array[pivotIndex] = tmp;

	unsigned int storeIndex = left;

	// Put items < than the pivot before the pivot index
	unsigned int i;
	for (i = left; i < right; ++i) {
		if (array[i] < pivotValue) {
			tmp = array[i];
			array[i] = array[storeIndex];
			array[storeIndex] = tmp;
			++storeIndex;
		}
	}

	// Then put the pivot back (swap first number larger than pivot)
	tmp = array[right];
	array[right] = array[storeIndex];
	array[storeIndex] = tmp;
	return storeIndex;
}

void quicksortUInt(unsigned int* array, unsigned int left, unsigned int right)
{
	if (left < right) {
		// Choose a new pivot somewhere between right and left
		unsigned int pivotIndex = left + (genrand_int32() % (right - left));

		// Partition the array
		unsigned int newPivotIndex = partitionUInt(array, left, right, pivotIndex);

		// Recursively sort
		if (newPivotIndex > 0) {
			quicksortUInt(array, left, newPivotIndex - 1);
		}

		quicksortUInt(array, newPivotIndex + 1, right);
	}
}
