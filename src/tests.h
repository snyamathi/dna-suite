#ifndef TESTS_H_INCLUDED
#define TESTS_H_INCLUDED

void runTrivialTest();

void runBwtTest();

void runHashTest();

void runTrivialReference();

void runHashReference();

void runBwtReference();

#endif
