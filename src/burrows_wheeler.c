#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <memory.h>
#include "sn_errors.h"
#include "genome_utils.h"
#include "burrows_wheeler.h"
#include "listutils.h"
#include "mt19937-64.h"
#include "constants.h"
#include <math.h>

int compareGenomePointers(genomeDebug* g, const unsigned int numberOfPointers, unsigned int arg1, unsigned int arg2)
{
	assert(arg1 != arg2);

	unsigned int i;
	for (i = 0; i < g->genomeLength; ++i) {
		// End of genome is lexicographically lowest char.
		if (arg1 == g->genomeLength) {
			return true;
		} else if (arg1 == g->genomeLength) {
			return false;
		} else if (g->genomeString[arg1] != g->genomeString[arg2]) {
			return g->genomeString[arg1] < g->genomeString[arg2];
		}

		++arg1;
		++arg2;
	}

	// You shouln't get to here, a tie is NOT possible
	fputs("ERR: Sequence compare failed", stderr);
	exit(1);
	return false;
}

void swap(unsigned int* genomePointers, int arg1, int arg2)
{
	unsigned int tmp = genomePointers[arg1];
	genomePointers[arg1] = genomePointers[arg2];
	genomePointers[arg2] = tmp;
}

unsigned int partition(genomeDebug* g, const unsigned int numberOfPointers, unsigned int* genomePointers, unsigned int left, unsigned int right, unsigned int pivotIndex)
{
	unsigned int pivotValue;
	unsigned int storeIndex;

	pivotValue = genomePointers[pivotIndex];
	swap(genomePointers, pivotIndex, right);
	storeIndex = left;

	// Put items < than the pivot before the pivot index
	unsigned int i;
	for (i = left; i < right; ++i) {
		if (compareGenomePointers(g, numberOfPointers, genomePointers[i], pivotValue)) {
			swap(genomePointers, i, storeIndex);
			++storeIndex;
		}
	}

	// Then put the pivot back (swap first number larger than pivot)
	swap(genomePointers, right, storeIndex);
	return storeIndex;
}

void quicksort(genomeDebug* g, const unsigned int numberOfPointers, unsigned int* genomePointers, unsigned int left, unsigned int right)
{
	if (left < right) {
		// Choose a new pivot somewhere between right and left
		unsigned int pivotIndex = left + (genrand_int32() % (right - left));

		// Partition the array
		unsigned int newPivotIndex = partition(g, numberOfPointers, genomePointers, left, right, pivotIndex);

		// Recursively sort
		if (newPivotIndex > 0) {
			quicksort(g, numberOfPointers, genomePointers, left, newPivotIndex - 1);
		}
		quicksort(g, numberOfPointers, genomePointers, newPivotIndex + 1, right);
	}
}

void sortLeaf(genomeDebug* g, const unsigned int numberOfPointers, unsigned int* genomePointers)
{
	if (numberOfPointers == 0) {
		return;
	}
	quicksort(g, numberOfPointers, genomePointers, 0, numberOfPointers-1);
}

void bwt(genomeDebug* g)
{
	int i;
	unsigned int* genomePointers;
	bwtDebugItem* lastColumn;
	bwtDebugItem* firstColumn;

	lastColumn = (bwtDebugItem*) malloc(g->genomeLength * sizeof(bwtDebugItem));
	checkMalloc(lastColumn);
	firstColumn = (bwtDebugItem*) malloc(g->genomeLength * sizeof(bwtDebugItem));
	checkMalloc(firstColumn);

	genomePointers = (unsigned int*) malloc(g->genomeLength * sizeof(int));
	checkMalloc(genomePointers);
	for (i = 0; i < g->genomeLength; ++i) {
		genomePointers[i] = i;
	}

	sortLeaf(g, g->genomeLength, genomePointers);
	for (i = 0; i < g->genomeLength; ++i) {
		firstColumn[i].index = i;
		firstColumn[i].base = g->genomeString[genomePointers[i]];

		// Get the base one one base behind
		if (genomePointers[i] == 0) {
			lastColumn[i].index = g->genomeLength-1;
			lastColumn[i].base = g->genomeString[g->genomeLength-1];
		} else {
			lastColumn[i].index = genomePointers[i-1];
			lastColumn[i].base = g->genomeString[genomePointers[i-1]];
		}
	}

}

void getDeltaFromGenomeCompressed(const genomeDebug* genome, const char* bwtString, const bwtIndexItem* bwtCountIndex, const unsigned int* bwtLocationIndex, readDebug* read)
{
	unsigned int matchLocation;
	unsigned int matchAmount;
	read->mismatchCount = 0;

	matchLongCompressed(genome, bwtString, bwtCountIndex, bwtLocationIndex, read->sequence, read->sequenceLength, &matchLocation, &matchAmount);

	if (matchAmount == 0) {
		read->mismatchCount = INT_MAX;
		return;
	}

	int i;
	int firstDiffFound = false;
	for (i = 0; i < read->sequenceLength; ++i) {
		if (genome->genomeString[matchLocation + i] == read->sequence[i]) {
			continue;
		}

		if (!firstDiffFound) {
			read->snp1 = read->sequence[i];
			read->snp1Location = matchLocation + i;
			firstDiffFound = true;
		} else {
			read->snp2 = read->sequence[i];
			read->snp2Location = matchLocation + i;
		}

		++read->mismatchCount;
	}
}

unsigned int getIndexInGenome(const genomeDebug* genome, const char* bwtString, const bwtIndexItem* bwtCountIndex, const unsigned int* bwtLocationIndex, const unsigned int location)
{
	unsigned int offset = 0;
	unsigned int l = location;
	while (l % indexRate != 0) {
		l = walkLeftCompressed(genome, l, bwtString, bwtCountIndex);
		++offset;
	}

	return offset + bwtLocationIndex[l / indexRate];
}

void matchLongCompressed(const genomeDebug* genome, const char* bwtString, const bwtIndexItem* bwtCountIndex, const unsigned int* bwtLocationIndex, const char* query, const int queryLength, unsigned int* matchLocation, unsigned int* matchAmount)
{
	unsigned int** matchLocations;
	unsigned int numberOfMatches = 0;
	unsigned int totalNumberOfMatches = 0;
	matchLocations = (unsigned int**) malloc((queryLength-7) * sizeof(unsigned int*));
	checkMalloc(matchLocations);

	unsigned int* matchCounts;
	matchCounts = (unsigned int*) malloc((queryLength-7) * sizeof(unsigned int));
	checkMalloc(matchCounts);

	unsigned int i;
	for (i = 0; i < queryLength-7; ++i) {
		exactMatchCompressed(genome, bwtString, bwtCountIndex, query+i, 8, &matchLocations[i], &numberOfMatches);
		matchCounts[i] = numberOfMatches;
		totalNumberOfMatches += numberOfMatches;
	}

	if (totalNumberOfMatches == 0) {
		*matchAmount = 0;
		free(matchLocations);
		free(matchCounts);
		return;
	}

	unsigned int* masterList;
	masterList = (unsigned int*) malloc(totalNumberOfMatches * sizeof(unsigned int));
	checkMalloc(masterList);

	unsigned int j;
	unsigned int k;
	for (i = 0, j = 0; i < queryLength-7; ++i) {
		for (k = 0; k < matchCounts[i]; ++k) {
			masterList[j] = getIndexInGenome(genome, bwtString, bwtCountIndex, bwtLocationIndex, matchLocations[i][k]) - i;
			++j;
		}

		if (matchCounts[i] != 0) {
			free(matchLocations[i]);
		}
	}

	assert(j == totalNumberOfMatches);

	quicksortUInt(masterList, 0, totalNumberOfMatches-1);
	getModeOfListUInt(masterList, totalNumberOfMatches, matchLocation, matchAmount);

	free(matchLocations);
	free(matchCounts);
	free(masterList);
}

void bwtPartial(genomeDebug* g, const char base, unsigned int* offset, char* bwtString, bwtIndexItem* bwtIndex, unsigned int* bwtLocationIndex, unsigned int* aData, unsigned int* cData, unsigned int* gData, unsigned int* tData, unsigned int* countIndexPointer, unsigned int* locationIndexPointer)
{
	char bwtChar;
	unsigned int i;
	unsigned int* genomePointers;
	unsigned int numberOfPointers;

	printf("Sorting %c's\n", base);
	fflush(stdout);

	getLocationsOfBase(g, base, &genomePointers, &numberOfPointers);
	sortLeaf(g, numberOfPointers, genomePointers);

	for (i = 0; i < numberOfPointers; ++i) {
		if (genomePointers[i] == 0) {
			bwtChar = g->genomeString[g->genomeLength-1];
		} else {
			bwtChar = g->genomeString[genomePointers[i]-1];
		}

		(bwtString)[i + *offset] = bwtChar;

		if ((i + *offset) % indexRate == 0) {
			bwtLocationIndex[*locationIndexPointer] = genomePointers[i];
			++(*locationIndexPointer);
		}

		if ((i + *offset) % countRate == 0) {
			bwtIndexItem item;
			item.aData = *aData;
			item.cData = *cData;
			item.gData = *gData;
			item.tData = *tData;
			bwtIndex[*countIndexPointer] = item;
			++(*countIndexPointer);
		}

		switch (bwtChar) {
		case 'A':
			++(*aData);
			break;
		case 'C':
			++(*cData);
			break;
		case 'G':
			++(*gData);
			break;
		case 'T':
			++(*tData);
			break;
		}
	}

	*offset += numberOfPointers;
	free(genomePointers);
}

void bwtFull(genomeDebug* g, char** bwtString, bwtIndexItem** bwtCountIndex, unsigned int** bwtLocationIndex)
{
	unsigned int aData = 0;
	unsigned int cData = 0;
	unsigned int gData = 0;
	unsigned int tData = 0;
	unsigned int countIndexPointer = 0;
	unsigned int locationIndexPointer = 0;


	unsigned int offset = 0;
	unsigned int countIndexLength = ceil(g->genomeLength / (double) countRate);
	unsigned int locationIndexLength = ceil(g->genomeLength / (double) indexRate);

	*bwtCountIndex = (bwtIndexItem*) malloc(countIndexLength * sizeof(bwtIndexItem));
	checkMalloc(*bwtCountIndex);

	*bwtLocationIndex = (unsigned int*) malloc(locationIndexLength * sizeof(unsigned int));
	checkMalloc(*bwtLocationIndex);

	*bwtString = (char*) malloc(g->genomeLength * sizeof(char));
	checkMalloc(*bwtString);

	bwtPartial(g, '$', &offset, *bwtString, *bwtCountIndex, *bwtLocationIndex, &aData, &cData, &gData, &tData, &countIndexPointer, &locationIndexPointer);
	bwtPartial(g, 'A', &offset, *bwtString, *bwtCountIndex, *bwtLocationIndex, &aData, &cData, &gData, &tData, &countIndexPointer, &locationIndexPointer);
	bwtPartial(g, 'C', &offset, *bwtString, *bwtCountIndex, *bwtLocationIndex, &aData, &cData, &gData, &tData, &countIndexPointer, &locationIndexPointer);
	bwtPartial(g, 'G', &offset, *bwtString, *bwtCountIndex, *bwtLocationIndex, &aData, &cData, &gData, &tData, &countIndexPointer, &locationIndexPointer);
	bwtPartial(g, 'T', &offset, *bwtString, *bwtCountIndex, *bwtLocationIndex, &aData, &cData, &gData, &tData, &countIndexPointer, &locationIndexPointer);

	assert(offset == g->genomeLength);
	if (countIndexPointer != countIndexLength) {
		printf("ERR: countIndexPointer=%u,countIndexLength=%u\n", countIndexPointer, countIndexLength);
		fflush(stdout);
	}
	assert(countIndexPointer == countIndexLength);
	assert(locationIndexPointer == locationIndexLength);
}

void getDeltaFromGenome(const genomeDebug* genome, const bwtDebugItem* fmIndex, readDebug* read)
{
	unsigned int matchLocation;
	unsigned int matchAmount;
	matchLong(genome, fmIndex, read->sequence, read->sequenceLength, &matchLocation, &matchAmount);

	int i;
	int firstDiffFound = false;
	for (i = 0; i < read->sequenceLength; ++i) {
		if (genome->genomeString[matchLocation + i] == read->sequence[i]) {
			continue;
		}

		if (!firstDiffFound) {
			read->snp1 = read->sequence[i];
			read->snp1Location = matchLocation + i;
			firstDiffFound = true;
		} else {
			read->snp2 = read->sequence[i];
			read->snp2Location = matchLocation + i;
		}

		++read->mismatchCount;
	}
}

void matchLong(const genomeDebug* genome, const bwtDebugItem* fmIndex, const char* query, const int queryLength, unsigned int* matchLocation, unsigned int* matchAmount)
{
	unsigned int** matchLocations;
	unsigned int numberOfMatches = 0;
	unsigned int totalNumberOfMatches = 0;
	matchLocations = (unsigned int**) malloc((queryLength-7) * sizeof(unsigned int*));

	unsigned int* matchCounts;
	matchCounts = (unsigned int*) malloc((queryLength-7) * sizeof(unsigned int));

	checkMalloc(matchLocations);

	int i;
	for (i = 0; i < queryLength-7; ++i) {
		exactMatch2(genome, fmIndex, query+i, 8, &matchLocations[i], &numberOfMatches);
		matchCounts[i] = numberOfMatches;
		totalNumberOfMatches += numberOfMatches;
	}

	unsigned int* masterList;
	masterList = (unsigned int*) malloc(totalNumberOfMatches * sizeof(unsigned int));
	checkMalloc(masterList);

	unsigned int j = 0;
	unsigned int k;
	for (i = 0; i < queryLength-7; ++i) {
		for (k = 0; k < matchCounts[i]; ++k) {
			masterList[j] = fmIndex[matchLocations[i][k]].index - i;
			++j;
		}

		if (matchCounts[i] != 0) {
			free(matchLocations[i]);
		}
	}

	assert(j == totalNumberOfMatches);

	quicksortUInt(masterList, 0, totalNumberOfMatches-1);
	getModeOfListUInt(masterList, totalNumberOfMatches, matchLocation, matchAmount);

	free(matchLocations);
	free(matchCounts);
	free(masterList);
}

char baseAtOffsetFromFmIndex(const char* genome, const unsigned int genomeLength, const bwtDebugItem item, const unsigned int offset)
{
	int location = item.index + offset;
	if (location >= genomeLength) {
		location -= genomeLength;
	}

	return genome[location];
}

unsigned int walkLeftCompressed(const genomeDebug* genome, unsigned int currentBaseLocation, const char* bwtString, const bwtIndexItem* bwtCountIndex)
{
	char toFind = bwtString[currentBaseLocation];
	unsigned int count = 1;
	unsigned int i;
	unsigned int charAbove = 0; // The dollar sign is above all.

	// Get the first indexed location above this position in the BWT string
	// Count what we pass on the way up though.
	for (i = 0; i < currentBaseLocation % countRate; ++i) {
		if (bwtString[currentBaseLocation - (i + 1)] == toFind) {
			++count;
		}
	}

	switch(toFind) {
	case '$':
		return 0;
	case 'A':
		charAbove = count + bwtCountIndex[currentBaseLocation / countRate].aData;
		return charAbove;
		break;
	case 'C':
		charAbove = count + bwtCountIndex[currentBaseLocation / countRate].cData;
		return charAbove + genome->countA;
		break;
	case 'G':
		charAbove = count + bwtCountIndex[currentBaseLocation / countRate].gData;
		return charAbove + genome->countA + genome->countC;
		break;
	case 'T':
		charAbove = count + bwtCountIndex[currentBaseLocation / countRate].tData;
		return charAbove + genome->countA + genome->countC + genome->countG;
		break;
	}

	fputs("ERR: Walk left failed.", stderr);
	exit(5);
	return UINT_MAX;
}

void exactMatchCompressed(const genomeDebug* genome, const char* bwtString, const bwtIndexItem* bwtCountIndex, const char* query, const int queryLength, unsigned int** matchLocations, unsigned int* numberOfMatches)
{
	int i;
	*numberOfMatches = 0;
	unsigned int low = 0;
	unsigned int high = genome->genomeLength-1;

	for (i = queryLength - 1; i >= 0; --i) {
		while (query[i] != bwtString[low]) {
			++low;
			if (low == genome->genomeLength || low > high) {
				*numberOfMatches = 0;
				return;
			}
		}

		while (query[i] != bwtString[high]) {
			if (high == 0 || low > high) {
				*numberOfMatches = 0;
				return;
			}
			--high;
		}

		low = walkLeftCompressed(genome, low, bwtString, bwtCountIndex);
		high = walkLeftCompressed(genome, high, bwtString, bwtCountIndex);
	}

	*numberOfMatches = high - low + 1;
	*matchLocations = (unsigned int*) malloc((*numberOfMatches) * sizeof(unsigned int));
	checkMalloc(*matchLocations);

	for (i = 0; i < *numberOfMatches; ++i) {
		(*matchLocations)[i] = low+i;
	}

	assert(high >= low);
	assert(*numberOfMatches != 0);

}

void exactMatch2(const genomeDebug* genome, const bwtDebugItem* fmIndex, const char* query, const int queryLength, unsigned int** matchLocations, unsigned int* numberOfMatches)
{
	int i;
	*numberOfMatches = 0;
	unsigned int low = 0;
	unsigned int high = genome->genomeLength-1;

	for (i = queryLength - 1; i >= 0; --i) {
		while (query[i] != fmIndex[low].base) {
			++low;
			if (low == genome->genomeLength || low > high) {
				*numberOfMatches = 0;
				return;
			}
		}
		low = walkLeft(genome, low, fmIndex);

		while (query[i] != fmIndex[high].base) {
			if (high == 0 || low > high) {
				*numberOfMatches = 0;
				return;
			}
			--high;
		}
		high = walkLeft(genome, high, fmIndex);
	}

	*numberOfMatches = high - low + 1;
	*matchLocations = (unsigned int*) malloc((*numberOfMatches) * sizeof(unsigned int));
	checkMalloc(*matchLocations);

	for (i = 0; i < *numberOfMatches; ++i) {
		(*matchLocations)[i] = low+i;
	}

	assert(high >= low);
	assert(*numberOfMatches != 0);

}

void exactMatchDebug(const char* genome, const unsigned int genomeLength, const bwtDebugItem* fmIndex, const char* query, const int queryLength, unsigned int** matchLocations, unsigned int* numberOfMatches)
{
	int i;
	*numberOfMatches = 0;
	unsigned int low = 0;
	unsigned int high = genomeLength-1;

	for (i = 0; i < queryLength; ++i) {
		while (query[i] != baseAtOffsetFromFmIndex(genome, genomeLength, fmIndex[low], i)) {
			++low;
			if (low == genomeLength) {
				*numberOfMatches = 0;
				return;
			}
		}

		while (query[i] != baseAtOffsetFromFmIndex(genome, genomeLength, fmIndex[high], i)) {
			if (high == 0) {
				*numberOfMatches = 0;
				return;
			}
			--high;
		}

		if (low == high) {
			break;
		}
	}

	*numberOfMatches = high - low + 1;
	*matchLocations = (unsigned int*) malloc((*numberOfMatches) * sizeof(unsigned int));
	checkMalloc(*matchLocations);
	for (i = 0; i < *numberOfMatches; ++i) {
		(*matchLocations)[i] = fmIndex[low+i].index;
	}
}

//void processPartialBwt(const unsigned int totalNumberOfPointers, const unsigned int offset, const bwtDebugItem* fmIndex, char* bwtString, bwtIndexItem* bwtIndex, unsigned int* indexLength)
//{
//	unsigned int i;
//	for (i = 0; i < offset; ++i) {
//		bwtString[totalNumberOfPointers + i] = fmIndex[i].index;
//		if (((totalNumberOfPointers + i) % compressionFactor) == 0) {
//			bwtIndex[*indexLength].index = fmIndex[i].index;
//			bwtIndex[*indexLength].aData = fmIndex[i].aData;
//			bwtIndex[*indexLength].cData = fmIndex[i].cData;
//			bwtIndex[*indexLength].gData = fmIndex[i].gData;
//			bwtIndex[*indexLength].tData = fmIndex[i].tData;
//			++(*indexLength);
//		}
//	}
//}

void bwtDebug(const char* genome, const unsigned int genomeLength, unsigned int* genomePointers, unsigned int numberOfPointers, bwtDebugItem** fmIndex)
{
	unsigned int aData = 0;
	unsigned int cData = 0;
	unsigned int gData = 0;
	unsigned int tData = 0;

	*fmIndex = (bwtDebugItem*) malloc(genomeLength * sizeof(bwtDebugItem));
	checkMalloc(*fmIndex);

	char* bwtString;
	getBwtString(genome, genomeLength, genomePointers, &bwtString);

	int i;
	for (i = 0; i < genomeLength; ++i) {
		(*fmIndex)[i].base = bwtString[i];
		(*fmIndex)[i].aData = aData;
		(*fmIndex)[i].cData = cData;
		(*fmIndex)[i].gData = gData;
		(*fmIndex)[i].tData = tData;
		(*fmIndex)[i].index = genomePointers[i];

		switch (bwtString[i]) {
		case 'A':
			++aData;
			break;
		case 'C':
			++cData;
			break;
		case 'G':
			++gData;
			break;
		case 'T':
			++tData;
			break;
		}
	}
}

unsigned int walkLeft(const genomeDebug* genome, unsigned int currentBaseLocation, const bwtDebugItem* fmIndex)
{
	unsigned int charAbove = 0;

	switch(fmIndex[currentBaseLocation].base) {
	case 'A':
		charAbove = fmIndex[currentBaseLocation].aData;
		return charAbove;
		break;
	case 'C':
		charAbove = fmIndex[currentBaseLocation].cData;
		return genome->countA + charAbove;
		break;
	case 'G':
		charAbove = fmIndex[currentBaseLocation].gData;
		return genome->countA + genome->countC + charAbove;
		break;
	case 'T':
		charAbove = fmIndex[currentBaseLocation].tData;
		return genome->countA + genome->countC + genome->countG + charAbove;
		break;
	}

	fputs("ERR: Walk left failed.", stderr);
	exit(5);
	return UINT_MAX;
}

void getBwtString(const char* genome, const unsigned int genomeLength, unsigned int* genomePointers, char** bwtString)
{
	int i;

	*bwtString = (char*) malloc(genomeLength * sizeof(char));
	checkMalloc(bwtString);

	for (i = 0; i < genomeLength; ++i) {
		if (genomePointers[i] == 0) {
			(*bwtString)[i] = genome[genomeLength-1];
		} else {
			(*bwtString)[i] = genome[genomePointers[i]-1];
		}
	}
}
