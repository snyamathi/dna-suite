#include "constants.h"

const char* testGenomeFilepath = "D:\\TEMP\\testGenome.fa";

const char* readsFilepath = "D:\\TEMP\\reads.fa";

const char* octetCountsFilepath = "D:\\TEMP\\octetCounts.txt";

const char* randomGenomeFilepath100M = "D:\\TEMP\\randomGenome100M.fa";

const char* randomGenomeFilepath10M = "D:\\TEMP\\randomGenome10M.fa";

const char* randomGenomeFilepath1M = "D:\\TEMP\\randomGenome1M.fa";

const char* randomGenomeFilepath10K = "D:\\TEMP\\randomGenome10K.fa";

const char* logFilePath = "C:\\Users\\suneil\\Desktop\\logfile.csv";

int countRate = 0;

int indexRate = 0;
