#ifndef BURROWS_WHEELER_H_INCLUDED
#define BURROWS_WHEELER_H_INCLUDED

typedef struct {
	char base0        : 2;
	char base1        : 2;
	char base2        : 2;
	char base3        : 2;
	char base4        : 2;
	char base5        : 2;
	char base6        : 2;
	char base7        : 2;
	char base8        : 2;
	char base9        : 2;
	char base10       : 2;
	char base11       : 2;
	char base12       : 2;
	char base13       : 2;
	char base14       : 2;
	unsigned int type : 2;
	unsigned int data : 32;
} bwtItem;

typedef struct {
	char base;
	unsigned int type;
	unsigned int aData;
	unsigned int cData;
	unsigned int gData;
	unsigned int tData;
	unsigned int index;
} bwtDebugItem;

typedef struct {
	unsigned int aData;
	unsigned int cData;
	unsigned int gData;
	unsigned int tData;
} bwtIndexItem;

unsigned int walkLeftCompressed(const genomeDebug* genome, unsigned int currentBaseLocation, const char* bwtString, const bwtIndexItem* bwtCountIndex);

void bwtFull(genomeDebug* g, char** bwtString, bwtIndexItem** bwtCountIndex, unsigned int** bwtLocationIndex);

void getDeltaFromGenomeCompressed(const genomeDebug* genome, const char* bwtString, const bwtIndexItem* bwtCountIndex, const unsigned int* bwtLocationIndex, readDebug* read);

void exactMatchCompressed(const genomeDebug* genome, const char* bwtString, const bwtIndexItem* bwtCountIndex, const char* query, const int queryLength, unsigned int** matchLocations, unsigned int* numberOfMatches);

void matchLongCompressed(const genomeDebug* genome, const char* bwtString, const bwtIndexItem* bwtCountIndex, const unsigned int* bwtLocationIndex, const char* query, const int queryLength, unsigned int* matchLocation, unsigned int* matchAmount);

void processPartialBwt(const unsigned int totalNumberOfPointers, const unsigned int offset, const bwtDebugItem* fmIndex, char* bwtString, bwtIndexItem* bwtIndex, unsigned int* indexLength);

void bwtPartial(genomeDebug* g, const char base, unsigned int* offset, char* bwtString, bwtIndexItem* bwtIndex, unsigned int* bwtLocationIndex, unsigned int* aData, unsigned int* cData, unsigned int* gData, unsigned int* tData, unsigned int* countIndexPointer, unsigned int* locationIndexPointer);

void exactMatch3(const genomeDebug* genome, const bwtDebugItem* fmIndex, const char* query, const int queryLength, unsigned int* matchLocations, unsigned int* numberOfMatches);

void getDeltaFromGenome(const genomeDebug* genome, const bwtDebugItem* fmIndex, readDebug* read);

void matchLong(const genomeDebug* genome, const bwtDebugItem* fmIndex, const char* query, const int queryLength, unsigned int* matchLocation, unsigned int* matchAmount);

void exactMatchDebug(const char* genome, const unsigned int genomeLength, const bwtDebugItem* fmIndex, const char* query, const int queryLength, unsigned int** matchLocations, unsigned int* numberOfMatches);

void bwtDebug(const char* genome, const unsigned int genomeLength, unsigned int* genomePointers, unsigned int numberOfPointers, bwtDebugItem** fmIndex);

void exactMatch(const char* bwtString, const unsigned int bwtStringLength, const char* query, const unsigned int queryLength);

void getBwtString(const char* genome, const unsigned int genomeLength, unsigned int* genomePointers, char** bwtString);

void sortLeaf(genomeDebug* g, const unsigned int numberOfPointers, unsigned int* genomePointers);

unsigned int walkLeft(const genomeDebug* genome, unsigned int currentBaseLocation, const bwtDebugItem* fmIndex);

void exactMatch2(const genomeDebug* genome, const bwtDebugItem* fmIndex, const char* query, const int queryLength, unsigned int** matchLocations, unsigned int* numberOfMatches);

#endif
