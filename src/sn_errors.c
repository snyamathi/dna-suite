#include <stdio.h>
#include <stdlib.h>

void _checkMalloc(const void* pointer, const int lineNumber, const char* filename)
{
	if (pointer == NULL) {
		fprintf(stderr, "ERR: malloc failed on line %i of %s", lineNumber, filename);
		exit(1);
	}
}

void _checkFile(const void* pointer, const int lineNumber, const char* filename)
{
	if (pointer == NULL) {
		fprintf(stderr, "ERR: file open failed on line %i of %s", lineNumber, filename);
		exit(2);
	}
}

void _checkRead(const unsigned int observed, const unsigned int expected, const int lineNumber, const char* filename)
{
	if (observed != expected) {
		fprintf(stderr, "ERR: reading failed on line %i of %s", lineNumber, filename);
		exit (3);
	}
}
