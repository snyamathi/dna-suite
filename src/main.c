#include <stdio.h>
#include "mt19937-64.h"
#include "tests.h"
#include "genome_utils.h"

int main(void)
{
	init_genrand_time();
	runTrivialReference();	runHashReference();	runBwtReference();
	fputs("Done.", stdout);
	return 0;
}
